<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Test\Controller\Index' => 'Test\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'test' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/test[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Test\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'test/index/index' => __DIR__ . '/../view/test/test/index.phtml',
            'test/index/add' => __DIR__ . '/../view/test/test/add.phtml',
            'test/index/edit' => __DIR__ . '/../view/test/test/edit.phtml',
        ),
        'template_path_stack' => array(
            'test' => __DIR__ . '/../view',
        ),
    ),
);