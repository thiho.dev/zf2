<?php

namespace Test\Controller;

use Test\Form\TestForm;
use Test\Model\Test;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    protected $testTable;

    public function indexAction()
    {

        $tests = $this->getTestTable()->fetchAll(true);
        $tests->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));

        return new ViewModel(array(
            'tests' => $tests
        ));
    }

    public function addAction()
    {
        $form = new TestForm();
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $test = new Test();
            $form->setInputFilter($test->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $test->exchangeArray($form->getData());
                $this->getTestTable()->saveTest($test);

                // Redirect to list of albums
                return $this->redirect()->toRoute('test');
            }
        }
        return array('form' => $form);
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('test', array(
                'action' => 'add'
            ));
        }

        try {
            $test = $this->getTestTable()->getTest($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('test', array(
                'action' => 'index'
            ));
        }

        $form  = new TestForm();
        $form->bind($test);
        $form->get('submit')->setAttribute('value', 'Edit');
        $form->get('title')->setAttribute('value', $test->title);
        $form->get('id')->setAttribute('value', $test->id);
        $form->get('artist')->setAttribute('value', $test->artist);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($test->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getTestTable()->saveTest($test);

                return $this->redirect()->toRoute('test');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('test');
        }

        $this->getTestTable()->deleteTest($id);

        return $this->redirect()->toRoute('test');
    }

    public function getTestTable()
    {
        if (!$this->testTable) {
            $sm = $this->getServiceLocator();
            $this->testTable = $sm->get('Test\Model\TestTable');
        }
        return $this->testTable;
    }
}