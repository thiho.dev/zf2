<?php

namespace Test\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class TestTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = null)
    {
         if ($paginated) {
             $select = new Select('test');
             $resultSetPrototype = new ResultSet();
             $resultSetPrototype->setArrayObjectPrototype(new Test());
             $paginatorAdapter = new DbSelect(
                 $select,
                 $this->tableGateway->getAdapter(),
                 $resultSetPrototype
             );
             return new Paginator($paginatorAdapter);
         }

        return $this->tableGateway->select();
    }

    public function getTest($id)
    {
        $id  = (int) $id;
        $rowSet = $this->tableGateway->select(array('id' => $id));
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveTest(Test $test)
    {
        $data = array(
            'artist' => $test->artist,
            'title'  => $test->title,
        );

        $id = (int) $test->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            try {
                if ($this->getTest($id)) {
                    $this->tableGateway->update($data, array('id' => $id));
                } else {
                    throw new \Exception('Test id does not exist');
                }
            } catch (\Exception $e) {
            }
        }
    }

    public function deleteTest($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}